# REWRITE THIS SHIT

CFLAGS=--std=c99 -Iinclude `sdl2-config --libs --cflags` -g --std=c99 -Wall -lSDL2_image -lm
BIN_DIR=./bin

all: clientserver

$(BIN_DIR):
	if [ ! -d "./bin" ]; \
	then mkdir ./bin;	fi

clientserver: $(BIN_DIR)
	clang -o bin/server src/server/server.c src/server/chat.c $(CFLAGS)
	clang -o bin/client src/client/client.c $(CFLAGS)
