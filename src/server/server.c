#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <pthread.h>

#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>

#include "pong.h"
#include "server.h"

int server_on = TRUE;

int main (int argc, char **argv)
{
    struct sockaddr_in server_addr;
    int playeronesd, playertwosd, listensd;
    uint16_t port;

    pthread_t chat_thread;
    thread_info_stc thread_info;

    players_info_stc pinfo;
    mouse_info_stc playeroneminfo;
    mouse_info_stc playertwominfo;

    srand(clock());

    int player_speed = 300;
    int ball_speed = player_speed / 10;

    int playerone_y_vel = 0;
    int playertwo_y_vel = 0;

    int ball_y_vel = -10 + rand() % 20;
    int ball_x_vel = 10;

    int playerone_connected = FALSE;
    int playertwo_connected = FALSE;

    int counter = 0;

    int playerone_bytes_read;
    int playertwo_bytes_read;
    
    int target_y;
    float delta_y;
    float distance;

    if (argc != 3) {
        printf("Usage: %s server_port chat_port\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    port = atoi(argv[1]);
    printf("port = %u\n", port);
    thread_info.chat_port = atoi(argv[2]);

    listensd = socket(AF_INET, SOCK_STREAM, 0);

    if (listensd < 0) {
        perror("socket");
        exit(-1);
    }

    server_addr.sin_family      = AF_INET;
    server_addr.sin_port        = htons(port);
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(listensd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
        perror("bind");
        exit(-1);
    }

    if (listen(listensd, 4) != 0) {
        perror("listen");
        exit(-1);
    }

    pthread_create(&chat_thread, NULL,
        (void *) chat_server, &thread_info);

    pinfo.playerone_y = WINDOW_HEIGHT / 2 - 
        PLAYER_SPRITE_HEIGHT / 2;
    pinfo.playerone_x = WINDOW_WIDTH - 
        PLAYER_SPRITE_WIDTH;
    pinfo.playertwo_y = WINDOW_HEIGHT / 2 - 
        PLAYER_SPRITE_HEIGHT / 2;
    pinfo.playertwo_x = 0;
    pinfo.ball_y =  WINDOW_HEIGHT / 2 - 
        BALL_SPRITE_WIDTH / 2;
    pinfo.ball_x = WINDOW_WIDTH / 2 - 
        BALL_SPRITE_WIDTH / 2;

    while (server_on) {
        playeronesd = accept(listensd,  NULL, NULL);
        if (playeronesd < 0) {
            perror("accept");
            exit(-1);
        }
        playerone_connected = TRUE;

        printf("Player one connected\n");

        playertwosd = accept(listensd, NULL, NULL);
        if (playertwosd < 0) {
            perror("accept");
            exit(-1);
        }
        playertwo_connected = TRUE;

        printf("Player two connected\n");

        while(playerone_connected == TRUE && 
            playertwo_connected == TRUE) {
            while (pinfo.playerone_score != 3 && 
                pinfo.playertwo_score != 3) {
                playerone_bytes_read = recv(playeronesd,
                    &playeroneminfo, sizeof(playeroneminfo), 0);
                if (playerone_bytes_read <= 0) {
                    printf("Player one disconnected\n");
                    playerone_connected = FALSE;
                }
                playertwo_bytes_read = recv(playertwosd,
                    &playertwominfo, sizeof(playeroneminfo), 0);
                if (playertwo_bytes_read <= 0) {
                    printf("Player two disconnected\n");
                        playertwo_connected = FALSE;
                }

                target_y = playeroneminfo.y -
                    PLAYER_SPRITE_HEIGHT / 2;
                delta_y = target_y - pinfo.playerone_y;
                distance = fabs(delta_y);

                if (distance < 5) {
                    playerone_y_vel = 0;
                } else {
                    playerone_y_vel = delta_y *
                        player_speed / distance;
                }

                pinfo.playerone_y += playerone_y_vel / 60;

                if (pinfo.playerone_x <= 0) pinfo.playerone_x = 0;
                if (pinfo.playerone_y <= 0) pinfo.playerone_y = 0;
                if (pinfo.playerone_x >= WINDOW_WIDTH - 
                    PLAYER_SPRITE_WIDTH) pinfo.playerone_x = 
                    WINDOW_WIDTH - PLAYER_SPRITE_WIDTH;
                if (pinfo.playerone_y >= WINDOW_HEIGHT - 
                    PLAYER_SPRITE_HEIGHT) pinfo.playerone_y =
                    WINDOW_HEIGHT - PLAYER_SPRITE_HEIGHT;

                target_y = playertwominfo.y -
                    PLAYER_SPRITE_HEIGHT / 2;
                delta_y = target_y - pinfo.playertwo_y;
                distance = fabs(delta_y);

                if (distance < 5) {
                    playertwo_y_vel = 0;
                } else {
                    playertwo_y_vel = delta_y *
                        player_speed / distance;
                }

                pinfo.playertwo_y += playertwo_y_vel / 60;

                if (pinfo.playertwo_x <= 0) pinfo.playertwo_x = 0;
                if (pinfo.playertwo_y <= 0) pinfo.playertwo_y = 0;
                if (pinfo.playertwo_x >= WINDOW_WIDTH - 
                    PLAYER_SPRITE_WIDTH) pinfo.playertwo_x =
                    WINDOW_WIDTH - PLAYER_SPRITE_WIDTH;
                if (pinfo.playertwo_y >= WINDOW_HEIGHT - 
                    PLAYER_SPRITE_HEIGHT) pinfo.playertwo_y = 
                    WINDOW_HEIGHT - PLAYER_SPRITE_HEIGHT;

                pinfo.ball_x += (int) ball_speed * ball_x_vel / 60;
                pinfo.ball_y += (int) ball_speed * ball_y_vel / 60;

                if ((pinfo.ball_x <= BALL_SPRITE_WIDTH &&
                    pinfo.ball_y >= pinfo.playertwo_y - 
                    BALL_SPRITE_WIDTH &&
                    pinfo.ball_y <= pinfo.playertwo_y + 
                    PLAYER_SPRITE_HEIGHT) ||
                    (pinfo.ball_x >= WINDOW_WIDTH - 
                    PLAYER_SPRITE_WIDTH - BALL_SPRITE_WIDTH &&
                    pinfo.ball_y >= pinfo.playerone_y - 
                    BALL_SPRITE_WIDTH &&
                    pinfo.ball_y <= pinfo.playerone_y + 
                    PLAYER_SPRITE_HEIGHT)) {
                        if ((pinfo.ball_y >= pinfo.playerone_y - 
                        BALL_SPRITE_WIDTH && 
                        pinfo.ball_y < pinfo.playerone_y) ||
                            (pinfo.ball_y >= pinfo.playertwo_y - 
                            BALL_SPRITE_WIDTH && 
                            pinfo.ball_y < pinfo.playertwo_y)) {
                            ball_y_vel -= 2;
                        } else if ((pinfo.ball_y >= 
                            pinfo.playerone_y && pinfo.ball_y < 
                            pinfo.playerone_y + 
                            PLAYER_SPRITE_HEIGHT / 4) ||
                            (pinfo.ball_y >= pinfo.playertwo_y && 
                            pinfo.ball_y < pinfo.playertwo_y + 
                            PLAYER_SPRITE_HEIGHT / 4)) {
                            ball_y_vel -= 1;
                        } else if ((pinfo.ball_y >= 
                            pinfo.playerone_y + 
                            PLAYER_SPRITE_HEIGHT / 4 
                            && pinfo.ball_y < pinfo.playerone_y + 
                            PLAYER_SPRITE_HEIGHT / 2) ||
                            (pinfo.ball_y >= pinfo.playertwo_y + 
                            PLAYER_SPRITE_HEIGHT / 4 && 
                            pinfo.ball_y < 
                            pinfo.playertwo_y + 
                            PLAYER_SPRITE_HEIGHT / 2)){
                            ball_y_vel += 0;
                        } else if ((pinfo.ball_y >= 
                            pinfo.playerone_y + 
                            PLAYER_SPRITE_HEIGHT / 2 && 
                            pinfo.ball_y < pinfo.playerone_y + 
                            PLAYER_SPRITE_HEIGHT / 2 + 
                            PLAYER_SPRITE_HEIGHT / 4) ||
                            (pinfo.ball_y >= pinfo.playertwo_y + 
                            PLAYER_SPRITE_HEIGHT / 2 && 
                            pinfo.ball_y < 
                            pinfo.playertwo_y + 
                            PLAYER_SPRITE_HEIGHT / 2 + 
                            PLAYER_SPRITE_HEIGHT / 4)) {
                            ball_y_vel += 1;
                        } else if ((pinfo.ball_y >=
                            pinfo.playerone_y + 
                            PLAYER_SPRITE_HEIGHT / 2 + 
                            PLAYER_SPRITE_HEIGHT / 4 && 
                            pinfo.ball_y <= 
                            pinfo.playerone_y + 
                            PLAYER_SPRITE_HEIGHT) ||
                            (pinfo.ball_y >= 
                            pinfo.playertwo_y + 
                            PLAYER_SPRITE_HEIGHT / 2 + 
                            PLAYER_SPRITE_HEIGHT / 4 && 
                            pinfo.ball_y <= 
                            pinfo.playertwo_y + 
                            PLAYER_SPRITE_HEIGHT)) {
                            ball_y_vel += 2;
                        }
                        ball_x_vel *= -1;
                    }

                if (pinfo.ball_y <= 0 || pinfo.ball_y >= 
                    WINDOW_HEIGHT - BALL_SPRITE_WIDTH) {
                        ball_y_vel *= -1;
                }
                if (pinfo.ball_x <= 0 || pinfo.ball_x >= 
                    WINDOW_WIDTH - BALL_SPRITE_WIDTH) {
                    if (pinfo.ball_x <= 0) {
                        pinfo.playerone_score++;
                    } else {
                        pinfo.playertwo_score++;
                    }

                    pinfo.playerone_y = WINDOW_HEIGHT / 2 - 
                        PLAYER_SPRITE_HEIGHT / 2;
                    pinfo.playerone_x = WINDOW_WIDTH - 
                        PLAYER_SPRITE_WIDTH;
                    pinfo.playertwo_y = WINDOW_HEIGHT / 2 - 
                        PLAYER_SPRITE_HEIGHT / 2;
                    pinfo.playertwo_x = 0;
                    pinfo.ball_y =  WINDOW_HEIGHT/ 2 - 
                        BALL_SPRITE_WIDTH / 2;
                    pinfo.ball_x = WINDOW_WIDTH / 2 - 
                        BALL_SPRITE_WIDTH / 2;

                    player_speed = 300;
                    ball_speed = player_speed / 10;

                    playerone_y_vel = 0;
                    playertwo_y_vel = 0;

                    srand(clock());
                    ball_y_vel = -10 + (int) rand() % 20;
                    ball_y_vel = -10 + (int) rand() % 20;
                }

                counter++;
                if (counter >= 60) {
                    ball_speed++;
                    counter = 0;
                }

                send(playeronesd, &pinfo, sizeof(pinfo), 0);
                send(playertwosd, &pinfo, sizeof(pinfo), 0);
            }
            pinfo.playerone_score = 0;
            pinfo.playertwo_score = 0;
            pinfo.playerone_y = WINDOW_HEIGHT / 2 - 
                PLAYER_SPRITE_HEIGHT / 2;
            pinfo.playerone_x = WINDOW_WIDTH - 
                PLAYER_SPRITE_WIDTH;
            pinfo.playertwo_y = WINDOW_HEIGHT / 2 - 
                PLAYER_SPRITE_HEIGHT / 2;
            pinfo.playertwo_x = 0;
            pinfo.ball_y =  WINDOW_HEIGHT / 2 - 
                BALL_SPRITE_WIDTH / 2;
            pinfo.ball_x = WINDOW_WIDTH / 2 - 
                BALL_SPRITE_WIDTH / 2;

            player_speed = 300;
            ball_speed = player_speed / 10;

            playerone_y_vel = 0;
            playertwo_y_vel = 0;

            srand(clock());
            ball_y_vel = -10 + (int) rand() % 20;
            ball_x_vel = 10;
        }

        close(playeronesd);
        close(playertwosd);
    }
    server_on = FALSE;
    pthread_join(chat_thread, NULL);
}
