#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <pthread.h>

#include <sys/select.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>

#include "pong.h"
#include "server.h"

extern int server_on; 

void *chat_server(void *arg) {
    thread_info_stc *thread_info = arg;

    char message[MESSAGE_SIZE];
    int playeronesd, playertwosd, listensd;

    listensd = socket(AF_INET, SOCK_STREAM, 0);

    if (listensd < 0) {
        perror("(Chat) socket");
        exit(-1);
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;

    addr.sin_port = htons(thread_info->chat_port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(listensd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("(Chat) bind");
        exit(-1);
    }

    if (listen(listensd, 4) != 0) {
        perror("(Chat) bind");
        exit(-1);
    }

    playeronesd = accept(listensd,  NULL, NULL);
    if (playeronesd < 0) {
        perror("(Chat) accept");
        exit(-1);
    }

    printf("(Chat) player one connected\n");

    playertwosd = accept(listensd, NULL, NULL);
    if (playertwosd < 0) {
        perror("accept");
        exit(-1);
    }
    printf("(Chat) player two connected\n");

    while (server_on) {
        fd_set readsds;
        int ret;
        struct timeval tv;
        tv.tv_sec = TIMEOUT;
        tv.tv_usec = 0;

        FD_ZERO(&readsds);
        FD_SET(playeronesd, &readsds);
        FD_SET(playertwosd, &readsds);

        ret = select(playertwosd + 1, &readsds,
            NULL, NULL, &tv);

        if (ret < 0) {
            perror("(Chat) Select");
            server_on = FALSE;
        }

        if (ret == 0) {
            continue;
        }

        if (FD_ISSET(playeronesd, &readsds)) {
            int playerone_bytes_read = read(playeronesd,
                &message, sizeof(message));
            if (playerone_bytes_read <= 0) {
                printf("(Chat) player one disconnected, server shutdown \n");
                server_on = FALSE;
            }
            send(playeronesd, &message, sizeof(message), 0);
            send(playertwosd, &message, sizeof(message), 0);
        }
        if (FD_ISSET(playertwosd, &readsds)) {
            int playertwo_bytes_read = recv(playertwosd,
                &message, sizeof(message), 0);
            if (playertwo_bytes_read <= 0) {
                printf("(Chat) player two disconnected, server shutdown \n");
                server_on = FALSE;
            }
            send(playeronesd, &message, sizeof(message), 0);
            send(playertwosd, &message, sizeof(message), 0);
        }
    }

    return NULL;
}