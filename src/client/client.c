#include <unistd.h>
#include <stdio.h>
#include <math.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_image.h>
#include <pthread.h>
#include <sys/select.h>
#include "pong.h"

int close_requested = 0;
char playername[] = "player #: ";
char *ipaddr;

void *chat_client (void *arg)
{
    struct sockaddr_in addr;
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    thread_info_stc *thread_info = arg;

    if (sock < 0) {
        perror("(Chat) socket");
        exit(-1);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(thread_info->chat_port);
    
    if (!inet_aton(ipaddr, &(addr.sin_addr))) {
        perror("(Chat) IP");
    }
    
    if (connect(sock, (struct sockaddr *)&addr,
        sizeof(addr)) < 0) {
        perror("(Chat) connect");
        exit(-2);
    }
    
    int messages = 0;
    
    while (!close_requested) {
        char chat_buffer[10][MESSAGE_SIZE];
        char message[MESSAGE_SIZE];
        char temp_str[MESSAGE_SIZE];

        fd_set readsds;
        int ret;
        struct timeval tv;
        tv.tv_sec = TIMEOUT;
        tv.tv_usec = 0;

        FD_ZERO(&readsds);
        FD_SET(sock, &readsds);
        FD_SET(STDIN_FILENO, &readsds);

        ret = select(sock + 1, &readsds, NULL,
            NULL, &tv);

        if (ret < 0) {
            perror("Select");
        }

        if (ret == 0) {
            continue;
        }

        if (FD_ISSET(sock, &readsds)) {
            ret = recv(sock, &message,
                sizeof(message), 0);

            message[ret] = '\0';

            if (ret < 0) {
                perror("recv");
                continue;
            }

            if (messages >= 10) {
                for (int i = 1; i < messages; i++) {
                    memcpy(chat_buffer + i - 1,
                        chat_buffer + i, MESSAGE_SIZE);
                }
                memcpy(chat_buffer + 9,
                    message, MESSAGE_SIZE);

            } else {
                memcpy(chat_buffer + messages,
                    message, MESSAGE_SIZE);
                messages++;
            }
            printf("\E[H\E[2J");
            for (int i = 0; i < messages; i++){
                printf("%s", chat_buffer[i]);
            }
        }
        if (FD_ISSET(STDIN_FILENO, &readsds)) {
            ret = read(STDIN_FILENO, 
                &message, sizeof(message));
            for (int i = 0; i < 255; i++) {
                temp_str[i] = '\0';
            }
            memcpy(temp_str, playername,
                sizeof(playername) - 1);
            memcpy(temp_str + sizeof(playername) - 1,
                message, MESSAGE_SIZE - sizeof(playername));
            send(sock, temp_str, sizeof(temp_str), 0);
        }
        for (int i = 0; i < 255; i++) {
            message[i] = '\0';
        }
    }
    return NULL;
}

int main(int argc, char **argv)
{
    uint16_t port;
    thread_info_stc thread_info;

    if (argc < 5) {
        printf("Usage: %s ip port chat_port player_num \n", argv[0]);
        exit(-1);
    }

    ipaddr = argv[1];
    port = atoi(argv[2]);
    thread_info.chat_port = atoi(argv[3]);
    playername[7] = '0' + atoi(argv[4]);

    struct sockaddr_in addr;
    int sock = socket(AF_INET, SOCK_STREAM, 0);

    if (sock < 0) {
        perror("socket");
        exit(-1);   
    }

    printf("Connected to %s:%d\n", ipaddr, port);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);

    if (!inet_aton(ipaddr, &(addr.sin_addr))) {
        perror("IP");
    }

    if (connect(sock, (struct sockaddr *)&addr,
        sizeof(addr)) < 0) {
        perror("connect");
        exit(-2);
    }


    pthread_t chat_thread;

    pthread_create(&chat_thread,
        NULL,(void *) chat_client, &thread_info);


    players_info_stc pinfo;
    mouse_info_stc minfo;

    pinfo.playerone_y = 250;
    pinfo.playerone_x = 775;
    pinfo.playertwo_y = 250;
    pinfo.playertwo_x = 0;


    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0) {
        printf("error initializing SDL: %s\n", SDL_GetError());
        return 1;
    }

    SDL_Window* mainwindow = SDL_CreateWindow("Pong",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        WINDOW_WIDTH, WINDOW_HEIGHT, 0);

    if (mainwindow == NULL) {
        printf("error creating window: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    SDL_CaptureMouse(SDL_TRUE);

    SDL_Renderer* rend = SDL_CreateRenderer(mainwindow, -1,
        SDL_RENDERER_ACCELERATED);
    if (rend == NULL) {
      printf("error creating renderer: %s\n", SDL_GetError());
      SDL_DestroyWindow(mainwindow);
      SDL_Quit();
      return 1;
    }

    SDL_Surface* surface = IMG_Load("resources/playerone.png");
    if (surface == NULL) {
        printf("error creating surface\n");
        SDL_DestroyRenderer(rend);
        SDL_DestroyWindow(mainwindow);
        SDL_Quit();
        return 1;
    }

    SDL_Texture* playerone_texture =
        SDL_CreateTextureFromSurface(rend, surface);

    if (playerone_texture  == NULL) {
        printf("error creating player one texture: %s\n",
            SDL_GetError());
        SDL_DestroyRenderer(rend);
        SDL_DestroyWindow(mainwindow);
        SDL_Quit();
        return 1;
    }

    surface = IMG_Load("resources/playertwo.png");
    if (surface == NULL) {
        printf("error creating surface\n");
        SDL_DestroyRenderer(rend);
        SDL_DestroyWindow(mainwindow);
        SDL_Quit();
        return 1;
    }

    SDL_Texture* playertwo_texture =
        SDL_CreateTextureFromSurface(rend, surface);
    if (playertwo_texture  == NULL) {
        printf("error creating player two texture: %s\n",
            SDL_GetError());
        SDL_DestroyRenderer(rend);
        SDL_DestroyWindow(mainwindow);
        SDL_Quit();
        return 1;
    }

    surface = IMG_Load("resources/ball.png");
    if (surface == NULL) {
        printf("error creating surface\n");
        SDL_DestroyRenderer(rend);
        SDL_DestroyWindow(mainwindow);
        SDL_Quit();
        return 1;
    }

    SDL_Texture* ball_texture =
        SDL_CreateTextureFromSurface(rend, surface);
    SDL_FreeSurface(surface);
    if (playertwo_texture  == NULL) {
        printf("error creating player two texture: %s\n",
            SDL_GetError());
        SDL_DestroyRenderer(rend);
        SDL_DestroyWindow(mainwindow);
        SDL_Quit();
        return 1;
    }

    SDL_Rect playerone_dest;
    SDL_QueryTexture(playerone_texture, NULL, NULL,
        &playerone_dest.w, &playerone_dest.h);
    SDL_Rect playertwo_dest;
    SDL_QueryTexture(playertwo_texture, NULL, NULL,
        &playertwo_dest.w, &playertwo_dest.h);
    SDL_Rect ball_dest;
    SDL_QueryTexture(ball_texture, NULL, NULL,
        &ball_dest.w, &ball_dest.h);


    while (!close_requested) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                close_requested = 1;
            }
        }

        int mouse_x, mouse_y;
        SDL_GetMouseState(&mouse_x, &mouse_y);

        minfo.x = mouse_x;
        minfo.y = mouse_y;

        send(sock, &minfo, sizeof(minfo), 0);
        recv(sock, &pinfo, sizeof(pinfo), 0);

        playerone_dest.y = pinfo.playerone_y;
        playerone_dest.x = pinfo.playerone_x;
        playertwo_dest.y = pinfo.playertwo_y;
        playertwo_dest.x = pinfo.playertwo_x;
        ball_dest.y = pinfo.ball_y;
        ball_dest.x = pinfo.ball_x;

        SDL_RenderClear(rend);

        SDL_RenderCopy(rend, playerone_texture,
            NULL, &playerone_dest);
        SDL_RenderCopy(rend, playertwo_texture,
            NULL, &playertwo_dest);
        SDL_RenderCopy(rend, ball_texture,
            NULL, &ball_dest);
        SDL_RenderPresent(rend);

        SDL_Delay(100 / 6);
    }

    pthread_join(chat_thread, NULL);
    close(sock);
    SDL_DestroyTexture(playerone_texture);
    SDL_DestroyTexture(playertwo_texture);
    SDL_DestroyTexture(ball_texture);
    SDL_DestroyRenderer(rend);
    SDL_DestroyWindow(mainwindow);
    SDL_Quit();
}
