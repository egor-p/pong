#ifndef PONG_H
#define PONG_H

#define TRUE (1)
#define FALSE (0)

#define WINDOW_WIDTH          (800)
#define WINDOW_HEIGHT         (600)
#define PLAYER_SPRITE_WIDTH   (25)
#define PLAYER_SPRITE_HEIGHT  (100)
#define BALL_SPRITE_WIDTH     (25)
#define MESSAGE_SIZE          (255)
#define TIMEOUT               (2)

typedef struct players_info_stct {
	int playerone_x;
	int playerone_y;
	int playertwo_x;
	int playertwo_y;
	int ball_x;
	int ball_y;
	int playerone_score;
	int playertwo_score;
} players_info_stc;

typedef struct mouse_info_stct {
	int x;
	int y;
} mouse_info_stc;

typedef struct thread_info_stct {
    uint16_t chat_port;
	char *ipaddr;
} thread_info_stc;

#endif /* PONG_H */
